#include <Adafruit_NeoPixel.h>

#define digitalWriteFast(pin,val) (val ? sio_hw->gpio_set = (1 << pin) : sio_hw->gpio_clr = (1 << pin))
#define digitalReadFast(pin) ((1 << pin) & sio_hw->gpio_in)

#define RX1 26 // receive 1 pin
#define TX1 28 // transmit 1 pin
#define TX2 29 // transmit 2 pin
#define TX3 6  // transmit 3 pin

#define N_TX 3

#define settle 20 // settle time
#define samples 100 // number of samples to accumulate

#define USE_MIDI

// MIDI
#ifdef USE_MIDI
#include <Arduino.h>
#include <Adafruit_TinyUSB.h>
#include <MIDI.h>
Adafruit_USBD_MIDI usb_midi;
MIDI_CREATE_INSTANCE(Adafruit_USBD_MIDI, usb_midi, MIDI);
int rx_notes[N_TX] = {90, 93, 102};
#endif

// NEOPIXEL
Adafruit_NeoPixel pixels(1, 12, NEO_GRB + NEO_KHZ800);
int colors[N_TX][3] = {{255, 26, 0}, {0, 255, 0}, {0, 16, 255}};

// CAPACITIVE SENSING
int tx_pins[N_TX] = {TX1, TX2, TX3};
int32_t rx_values[N_TX];
int32_t rx_thresholds[N_TX] = {150, 190, 190};
bool rx_triggered[N_TX];

void setup() {
  pixels.begin();
  Serial.begin(0);

  pinMode(11, OUTPUT);
  digitalWrite(11, HIGH);

  for (int i = 0; i < N_TX; i++) {
    rx_triggered[N_TX] = false;
  }

  #ifdef USE_MIDI
  TinyUSB_Device_Init(0);
  MIDI.begin(MIDI_CHANNEL_OMNI);
  while( !TinyUSBDevice.mounted() ) delay(1);
  #endif
}

int32_t measure(int pin) {
  int32_t up, down;
  up = down = 0;
  pinMode(pin, OUTPUT);
  for (int i = 0; i < samples; ++i) {
    digitalWriteFast(pin, HIGH); // charge up
    up += analogRead(RX1); // read
    delayMicroseconds(settle); //settle
    digitalWriteFast(pin,LOW); // charge down
    down += analogRead(RX1); // read
    delayMicroseconds(settle); // settle
  }
  pinMode(pin, INPUT);
  return (up-down) / samples;
}

bool any = false;

void loop() {
  for (int i = 0; i < N_TX; i++) {
    rx_values[i] = measure(tx_pins[i]);
    if (rx_triggered[i]) {
      if (rx_values[i] < rx_thresholds[i]) {
        // release
        rx_triggered[i] = false;
        #ifdef USE_MIDI
        MIDI.sendNoteOff(64, 0, 1+i);
        #endif
      }
    } else {
      if (rx_values[i] > rx_thresholds[i]) {
        // pressed
        rx_triggered[i] = true;
        #ifdef USE_MIDI
        MIDI.sendNoteOn(64, 127, 1+i);
        #endif
      }
    }
  }

  bool any_now = false;

  for (int i = 0; i < N_TX; i++) {
    Serial.print(rx_values[i]);
    Serial.print(" ");
    any_now |= rx_triggered[i];
  }
  Serial.println("");

  // neopixel
  if (any_now && !any) {
    for (int i = 0; i < N_TX; i++) {
      if (rx_triggered[i]) {
        pixels.clear();
        pixels.setPixelColor(0, pixels.Color(colors[i][0], colors[i][1], colors[i][2]));
        pixels.show();
        break;
      }
    }
  }
  if (!any_now && any) {
    pixels.clear();
    pixels.setPixelColor(0, pixels.Color(0, 0, 0));
    pixels.show();
  }

  // update
  any = any_now;
}
